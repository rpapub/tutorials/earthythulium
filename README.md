
# Big Picture REFramework and software quality

The REFramework is the most prominent templates that come with the UiPath Studio, its use part of any advanced UiPath RPA Developer training, and sometimes it is even a bit mystified, too. For the RPA practitioner it is important to use the REFramework to its full potential regarding major software quality features.

@see: https://iso25000.com/index.php/en/iso-25000-standards/iso-25010


This repository contains some code experiments with the REFramework, meant not for productive use but to highlight a few potential adoptions.


## Maturity

The unmaintained repository on GitHub might be the best known location, but the current versions are located in a relatively unknown GitHub repository. And there are several other sources of adopted versions.

- source code locations and evolution

  - official (2x!)
    - https://github.com/UiPath/ReFrameWork
	- https://github.com/UiPath-Services/StudioTemplates/tree/develop/REFramework/contentFiles/any/any
  - Marketplace
    - https://marketplace.uipath.com/listings?sort=newest&query=reframework
  - others
    - https://github.com/rpapub/REFramework-Excel

- documentation

Due to its popularity the REFramework is frequently explained in YouTube videos, online courses and elsewhere.

 - PDF!
 - https://thelastweek.in/rpa/tagged/reframework
 - https://www.udemy.com/course/uipath-reframework-everything-explained/


## Modifiability

The default argument value when invoking InitAllSettings.xaml is a  powerful place to override configitems, and an input argument to the entrypoint makes dev- and prod-runs during hypercare easy:

- Configuration
  - https://gitlab.com/rpapub/tutorials/earthythulium

## Reusability

- multiple entry points


## Reliability

The UiPath Studio might actually prevent a clear picture on the inner workings. Here is a link to some visualizations that shall explain the retry behaviour.

- Fault Tolerance
  - BRE and Sys.Ecp and the Business Analyst
  - retry behaviour (-> Lucid )
    - https://lucid.app/lucidchart/9332eba6-cd16-4a9e-b9d6-e155a86116ab/edit?invitationId=inv_4b7a5297-d371-43a2-b8ea-5e1f7f17dbad

## Testability

-  wrap entrypoints in TestCase with e.g. in_ConfigFile


# Original README.md content

### Documentation is included in the Documentation folder ###

[REFrameWork Documentation](https://github.com/UiPath/ReFrameWork/blob/master/Documentation/REFramework%20documentation.pdf)

### REFrameWork Template ###
**Robotic Enterprise Framework**

* Built on top of *Transactional Business Process* template
* Uses *State Machine* layout for the phases of automation project
* Offers high level logging, exception handling and recovery
* Keeps external settings in *Config.xlsx* file and Orchestrator assets
* Pulls credentials from Orchestrator assets and *Windows Credential Manager*
* Gets transaction data from Orchestrator queue and updates back status
* Takes screenshots in case of system exceptions


### How It Works ###

1. **INITIALIZE PROCESS**
 + ./Framework/*InitiAllSettings* - Load configuration data from Config.xlsx file and from assets
 + ./Framework/*GetAppCredential* - Retrieve credentials from Orchestrator assets or local Windows Credential Manager
 + ./Framework/*InitiAllApplications* - Open and login to applications used throughout the process

2. **GET TRANSACTION DATA**
 + ./Framework/*GetTransactionData* - Fetches transactions from an Orchestrator queue defined by Config("OrchestratorQueueName") or any other configured data source

3. **PROCESS TRANSACTION**
 + *Process* - Process trasaction and invoke other workflows related to the process being automated 
 + ./Framework/*SetTransactionStatus* - Updates the status of the processed transaction (Orchestrator transactions by default): Success, Business Rule Exception or System Exception

4. **END PROCESS**
 + ./Framework/*CloseAllApplications* - Logs out and closes applications used throughout the process


### For New Project ###

1. Check the Config.xlsx file and add/customize any required fields and values
2. Implement InitiAllApplications.xaml and CloseAllApplicatoins.xaml workflows, linking them in the Config.xlsx fields
3. Implement GetTransactionData.xaml and SetTransactionStatus.xaml according to the transaction type being used (Orchestrator queues by default)
4. Implement Process.xaml workflow and invoke other workflows related to the process being automated
